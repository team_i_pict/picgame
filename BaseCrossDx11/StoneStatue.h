#pragma once
#include "stdafx.h"

namespace basecross
{
	//----------------------------------------------------------------------------
	//class StoneStatue : public GameObject
	//�l�X�ȍs��������Α�
	//----------------------------------------------------------------------------

	class StoneStatue : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//�\�z�Ɣj��
		StoneStatue(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);

		float Timer = 0;
		bool Hitflg = false;
		//������
		virtual ~StoneStatue();
		//����
		virtual void OnCreate()override;
		void OnUpdate();
	};
}
