/*!
@file Project.h
@brief コンテンツ用のヘッダをまとめる
*/

#pragma once


#include "resource.h"

#include "ProjectShader.h"
#include "Scene.h"
#include "GameStage.h"
#include "Character.h"
#include "Player.h"
#include "StoneStatue.h"
#include "MapObjects.h"
#include "Animal.h"


