#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	//----------------------------------------------------------------------------
	//class StoneStatue : public GameObject
	//様々な行動をする石像
	//----------------------------------------------------------------------------

	//構築と破棄
	StoneStatue::StoneStatue(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
	) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}

	StoneStatue::~StoneStatue() {}

	//初期化

	void StoneStatue::OnCreate()
	{
		auto PtrTransform = AddComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(true);
		auto PtrDraw = AddComponent<PNTStaticDraw>();

		//自分の色を白にする
		PtrDraw->SetDiffuse(Color4(1.0, 1.0, 1.0, 1.0));



		//影を付ける
		/*auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_SQUARE");*/

		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");

		PtrDraw->SetOwnShadowActive(true);
		PtrDraw->SetTextureResource(L"PICTSTAY_TX");

		auto Group = GetStage()->GetSharedObjectGroup(L"StoneStatue");
		Group->IntoGroup(GetThis<GameObject>());

		//透明処理
		SetAlphaActive(true);

	}
	void StoneStatue::OnUpdate()
	{
		//Actionの登録
		auto PtrAction = AddComponent<Action>();

		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//自分の色を見る
		auto MyColor = PtrDraw->GetDiffuse();

		//自分の色が青（Color4([R]0.0,[G]0.0,[B]1.0,[A]1.0)）になっているか見る
		if (MyColor == Color4(0.0, 0.0, 1.0, 1.0))
		{
			Timer += 1.0;
			if (Timer <= 1) {
				//アクションの登録
				PtrAction->AddMoveBy(2.0f, Vector3(-3.0f, 0.0f, 0));
				PtrAction->AddMoveBy(2.0f, Vector3(1.0f, 0.0f, 0));

				//ループする
				PtrAction->SetLooped(true);
				//アクション開始
				PtrAction->Run();
			}

			if (Timer >= 400)
			{
				PtrDraw->SetDiffuse(Color4(1.0, 1.0, 1.0, 1.0));
				PtrAction->Stop();
			}

		}
		if (Hitflg == true)
		{
			PtrDraw->SetDiffuse(Color4(0.0f, 0.0f, 1.0f, 1.0f));

		}

		//自分の色が赤（Color4([R]1.0,[G]0.0,[B]0.0,[A]1.0)）になっているか見る
		if (MyColor == Color4(1.0, 0.0, 0.0, 1.0))
		{
			Timer += 1.0;
			if (Timer <= 1) {
				PtrAction->AddMoveBy(2.0f, Vector3(-3.0f, 0.0f, 0));
				PtrAction->AddMoveBy(2.0f, Vector3(1.0f, 0.0f, 0));

				//ループする
				PtrAction->SetLooped(true);
				//アクション開始
				PtrAction->Run();
			}

			if (Timer >= 400)
			{
				PtrDraw->SetDiffuse(Color4(1.0, 1.0, 1.0, 1.0));
				auto PtrAction = AddComponent<Action>();

				//アクションの終了
				PtrAction->Stop();
			}

		}
		if (Hitflg == true)
		{
			PtrDraw->SetDiffuse(Color4(1.0f, 0.0f, 0.0f, 1.0f));
		}


		//自分の色が緑（Color4([R]0.0,[G]1.0,[B]0.0,[A]1.0)）になっているか見る
		if (MyColor == Color4(0.0, 1.0, 0.0, 1.0))
		{
			Timer += 1.0;
			if (Timer <= 1) {
				PtrAction->AddMoveBy(2.0f, Vector3(0.0f, 0.5f, 0));
				PtrAction->AddMoveBy(2.0f, Vector3(0.0f, -0.75f, 0));

				//ループする
				PtrAction->SetLooped(true);
				//アクション開始
				PtrAction->Run();

			}

			if (Timer >= 400)
			{
				PtrDraw->SetDiffuse(Color4(1.0, 1.0, 1.0, 1.0));
				auto PtrAction = AddComponent<Action>();

				//アクションの終了
				PtrAction->Stop();
				Hitflg = false;
				Timer = 0;
			}

		}
		if (Hitflg == true)
		{
			PtrDraw->SetDiffuse(Color4(0.0f, 1.0f, 0.0f, 1.0f));
		}

		auto PtrTransform = GetComponent<Transform>();
		auto PtrPos = PtrTransform->GetPosition();
		auto PtrScalse = PtrTransform->GetScale();

		//プレイヤーの取得
		auto PtrPlayer = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto PtrPlayerTrnasform = PtrPlayer->GetComponent<Transform>();
		auto PtrPlayerPos = PtrPlayerTrnasform->GetPosition();
		auto PtrPlayerScalse = PtrPlayerTrnasform->GetScale();

		//原点のを移動させる//右Xp//左Xm(Player)
		auto PtrPlayerXp = PtrPlayerPos.x;// +0.3;
		auto PtrPlayerXm = PtrPlayerPos.x;// -0.3;
		auto PtrPlayerYp = PtrPlayerPos.y;// +0.3;
		auto PtrPlayerYm = PtrPlayerPos.y;// -0.3;

										  //右Xp//左Xm
		auto PtrXp = PtrPos.x + (PtrScalse.x / 2);
		auto PtrXm = PtrPos.x - (PtrScalse.x / 2);
		auto PtrYp = PtrPos.y + (PtrScalse.y / 2);
		auto PtrYm = PtrPos.y - (PtrScalse.y / 2);

		if (PtrPlayerXp > PtrXm - 0.2 && PtrPlayerXm < PtrXp + 0.2  && PtrPlayerYp > PtrYm - 0.2 &&  PtrPlayerYm  < PtrYp + 0.2
			|| PtrPlayerXm  < PtrXm - 0.2 && PtrPlayerXp > PtrXp + 0.2 && PtrPlayerYp > PtrYm - 0.2 &&  PtrPlayerYm < PtrYp + 0.2) {

			Hitflg = true;
		}
	}
}