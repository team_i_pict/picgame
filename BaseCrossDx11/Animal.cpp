#include "stdafx.h"
#include "Project.h"

namespace basecross
{
	//-----------------------------------------------------
	//class Frog : public GameObject
	//蛙の動きをするオブジェクト
	//-----------------------------------------------------
	Frog::Frog(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{}

	//初期化
	Frog::~Frog(){}

	void Frog::OnCreate()
	{
		//蛙の初期設定
		auto TransPtr = GetComponent<Transform>();
		TransPtr->SetScale(0.1f, 0.1f, 0.1f);
		TransPtr->SetRotation(0.0f, 0.0f, 0.0f);
		TransPtr->SetPosition(3.5f, 0.125f, 0.0f);

		//Rigitbodyを付ける
		auto PtrRid = AddComponent<Rigidbody>();
		//重力を付ける
		auto PtrGra = AddComponent<Gravity>();

		//最下地点
		PtrGra->SetBaseY(-1.6f);
		//衝突判定を付ける
		auto PtrCol = AddComponent<Collision>();
		//横部分のみの反発（場合によっては消す可能性あり）
		PtrCol->SetIsHitAction(IsHitAction::AutoOnObjectRepel);

		/*影に関する処理*/
		//影を付ける
		//auto ShadowPtr = AddComponent<Shadowmap>();
		//影の形を設定
		//ShadowPtr->SetMeshResource(L"DEFAULT_SQUARE");

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<PNTStaticDraw>();
		//描画するメッシュの設定
		PtrDraw->SetMeshResource(L"DEFAULT_SQUARE");
		//テクスチャの設定
		PtrDraw->SetDiffuse(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrDraw->SetTextureResource(L"FROGSTAY_TX");
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//透明処理
		SetAlphaActive(true);

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<Frog> >(GetThis<Frog>());
		//初期ステートをFrogDefaultStateに設定
		m_StateMachine->ChangeState(FrogDefaultState::Instance());
	}

	//更新
	void Frog::OnUpdate()
	{
		//ステートマシンのUpdateを行う
		//この中でステートの更新が行われる(Execute()関数が呼ばれる)
		m_StateMachine->Update();
		this->GetComponent<Transform>()->SetRotation(/*GetComponent<Transform>()->GetRotation().x*/0, 0, 0);

		/*木にぶつかった処理*/
		auto PtrTransform = GetComponent<Transform>();
		auto PtrPos = PtrTransform->GetPosition();
		auto PtrScalse = PtrTransform->GetScale();

		//石像の取得
		auto Ptrtree = GetStage()->GetSharedObjectGroup(L"Tree");
		auto PtrtreeTrans = Ptrtree->at(0)->GetComponent<Transform>();
		auto PtrtreePos = PtrtreeTrans->GetPosition();
		auto PtrtreeScal = PtrtreeTrans->GetScale();

		//衝突による原点を移動させる//右Xp//左Xm(石像の原点を移動)
		auto PtrtreeXp = PtrtreePos.x;
		auto PtrtreeXm = PtrtreePos.x;
		auto PtrtreeYp = PtrtreePos.y;
		auto PtrtreeYm = PtrtreePos.y;

		auto PtrXp = PtrPos.x + (PtrScalse.x / 2);
		auto PtrXm = PtrPos.x - (PtrScalse.x / 2);
		auto PtrYp = PtrPos.y + (PtrScalse.y / 2);
		auto PtrYm = PtrPos.y - (PtrScalse.y / 2);

		if (PtrtreeXp > PtrXm - 0.2 && PtrtreeXm < PtrXp + 0.2 && PtrtreeYp > PtrYm - 0.2 && PtrtreeYm < PtrYp + 0.2
			|| PtrtreeXm < PtrXm - 0.2 && PtrtreeXp > PtrXp + 0.2 && PtrtreeYp > PtrYm - 0.2 && PtrtreeYm < PtrYp + 0.2)
		{
			FHitflg = true;
		}
		if (FHitflg == true)
		{
			auto PtrDraw = AddComponent<PNTStaticDraw>();
			PtrDraw->SetDiffuse(Color4(0.0f, 1.0f, 0.0f, 1.0f));
		}
	}

	/*ターンの最終更新
	void Frog::OnLastUpdate()
	{
		auto PtrCol = GetComponent<CollisionSphere>();
		auto
	}*/

	//モーションを実装する関数群
	//移動して向きを移動方向にする

	void Frog::MoveRotationMotion()
	{

	}

	bool Frog::IsLocateMotion()
	{
		//ディスタンスチェッカーの作成
		double distance_check = 3 * 3;
		//プレイヤーの一位置と蛙の位置を取る
		auto PtrTrans = AddComponent<Transform>();
		auto PtrPlayerTrans = GetStage()->AddGameObject<Player>()->AddComponent<Transform>();
		Vector3 FrogPos = PtrTrans->GetPosition();
		Vector3 PlayerPos = PtrPlayerTrans->GetPosition();

		if ((FrogPos.x - PlayerPos.x) * (FrogPos.x - PlayerPos.x)
			+ (FrogPos.y - PlayerPos.y) * (FrogPos.y - PlayerPos.y) <= distance_check)
		{
			return true;
		}
		return false;
	}

	//Playerが蛙に一定の距離近づくと逃げ出す
	void Frog::LocateMotion()
	{
		//ディスタンスチェッカーの作成
		double distance_check = 3 * 3;
		//プレイヤーの一位置と蛙の位置を取る
		auto PtrTrans = AddComponent<Transform>();
		auto PtrPlayerTrans = GetStage()->AddGameObject<Player>()->AddComponent<Transform>();
		Vector3 FrogPos = PtrTrans->GetPosition();
		Vector3 PlayerPos = PtrPlayerTrans->GetPosition();

		//distanceの作成
		if ((FrogPos.x - PlayerPos.x) * (FrogPos.x - PlayerPos.x) 
			+ (FrogPos.y - PlayerPos.y) * (FrogPos.y - PlayerPos.y) <= distance_check)
		{
			if (PlayerPos.x < FrogPos.x)
			{
				FrogPos.x += 0.5;
			}
			else if (PlayerPos.x > FrogPos.x)
			{
				FrogPos.x -= 0.5;
			}
		}
	}

	//時間がたった時にジャンプする瞬間の処理
	void Frog::JumpMotion()
	{
		float ElpsedTime = App::GetApp()->GetElapsedTime();

		auto PtrTrans = GetComponent<Transform>();
		//重力
		auto PtrGravity = GetComponent<Gravity>();
		if (ElpsedTime == 3)
		{
			//ジャンプスタート
			Vector3 JumpVec(0.3f, 0.5f, 0.0f);
			PtrGravity->StartJump(JumpVec, 0);
		}
		else if(ElpsedTime > 3)
		{
			ElpsedTime = 0;
		}
	}

	//ジャンプが終わったらtrueを返し、

	//-----------------------------------------------------------
	// class FrogDefaultState : public ObjState<Frog>
	// 用途:通常移動（ジャンプしながら横移動）
	//-----------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<FrogDefaultState>FrogDefaultState::Instance()
	{
		static shared_ptr < FrogDefaultState> instance;
		if (instance)
		{
			instance = shared_ptr<FrogDefaultState>(new FrogDefaultState);
		}
		return instance;
	}

	//ステートに入った時に呼ばれる関数
	void FrogDefaultState::Enter(const shared_ptr<Frog>& obj)
	{
		//何もしない
	}

	//ステート実行中に毎ターン呼ばれる関数
	void FrogDefaultState::Execute(const shared_ptr<Frog>& obj)
	{
		if (obj->IsLocateMotion())
		{
			//ステートの変更
		}
	}
	void FrogDefaultState::Exit(const shared_ptr<Frog>& obj)
	{
		//何もなし
	}

	//-----------------------------------------------------------
	// class FrogDefaultState : public ObjState<Frog>
	// 用途:通常移動（ジャンプしながら横移動）
	//-----------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<LocateState>LocateState::Instance()
	{
		static shared_ptr < LocateState> instance;
		if (instance)
		{
			instance = shared_ptr<LocateState>(new LocateState);
		}
		return instance;
	}

	//ステートに入った時に呼ばれる関数
	void LocateState::Enter(const shared_ptr<Frog>& obj)
	{
		//何もしない
	}

	//ステート実行中に毎ターン呼ばれる関数
	void LocateState::Execute(const shared_ptr<Frog>& obj)
	{
		//まだ入れない
	}
	void LocateState::Exit(const shared_ptr<Frog>& obj)
	{
		//何もなし
	}

}
