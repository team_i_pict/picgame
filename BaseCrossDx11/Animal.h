#pragma once
#include "stdafx.h"

namespace basecross {

	//------------------------------------------------------------
	//class Frog : public GameObject
	//蛙の動きをするオブジェクト
	//------------------------------------------------------------
	class Frog : public GameObject
	{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//構築と破棄
		Frog(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position);

		//蛙のHitFlg
		bool FHitflg = false;

		//初期化
		virtual ~Frog();
		//操作
		virtual void OnCreate()override;
		//更新
		virtual void OnUpdate()override;

		//ターンの最終更新時
		//virtual void OnLastUpdate() override;

		//蛙の向きを移動方向にする
		void MoveRotationMotion();

		//Playerを見つけた時の処理
		bool IsLocateMotion();

		//Playerを見つけた時の処理
		void LocateMotion();

		//一定時間たつとジャンプするかどうかを得る
		void JumpMotion();

		//ステートマシーン
		shared_ptr< StateMachine<Frog> >  m_StateMachine;

	};

	//------------------------------------------------
	// class FrogDefaultState : public ObjState<Frog>:
	// 用途:通常移動（ジャンプしながら横移動）
	//------------------------------------------------
	class FrogDefaultState : public ObjState<Frog>
	{
		FrogDefaultState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<FrogDefaultState>Instance();
		//ステートに入った時に呼ばれる関数
		virtual void Enter(const shared_ptr<Frog>& obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Frog>& obj)override;
		//ステートから抜ける時に呼ばれる関数
		virtual void Exit(const shared_ptr<Frog>& obj)override;
	};

	//-------------------------------------------------
	// class LocateState : public ObjState<Frog>;
	// 用途 : プレイヤーを見つけたら逃げる
	//-------------------------------------------------
	class LocateState : public ObjState<Frog>
	{
		LocateState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<LocateState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Frog>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Frog>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Frog>& Obj)override;
	};

}